# Licensing information

- The source code is released under the terms of the [GNU General Public License 3.0 or later](./LICENSES/GPL-3.0-or-later.txt).
- The original artwork and translations are released under the terms of the [Creative Commons Attribution-ShareAlike 4.0 International](./LICENSES/CC-BY-SA-4.0.txt).
- The website uses the [PT Sans](https://company.paratype.com/pt-sans-pt-serif) and [Work Sans](https://weiweihuanghuang.github.io/Work-Sans/) fonts, released under the terms of the [SIL Open Font License](./LICENSES/OFL-1.1.txt).

This project is [REUSE](https://reuse.software/)-compliant, each file's licensing information is annotated with [SPDX](https://spdx.dev/).
