# Allan Nordhøy <epost@anotheragency.no>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-10-24 23:48+0200\n"
"PO-Revision-Date: 2022-04-05 10:11+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/"
"metadata-cleaner/help/nb_NO/>\n"
"Language: nb_NO\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.12-dev\n"

#. (itstool) path: section/title
#: C/general.page:14
msgid "Metadata and privacy"
msgstr "Metadata og personvern"

#. (itstool) path: section/p
#: C/general.page:15
#, fuzzy
#| msgid ""
#| "Metadata consist of information that characterizes data. Metadata are "
#| "used to provide documentation for data products. In essence, metadata "
#| "answer who, what, when, where, why, and how about every facet of the data "
#| "that are being documented."
msgid ""
"Metadata consists of information that characterizes data. Metadata is used "
"to provide documentation for data products. In essence, metadata answers "
"who, what, when, where, why, and how about every facet of the data that is "
"being documented."
msgstr ""
"Metadata består av info som karakteriserer data. Det brukes til å angi "
"dokumentasjon om dataprodukter. I sum er det hvem, hva, når, hvor, hvorfor, "
"og hvordan om hvert aspekt av dataen som dokumenteres."

#. (itstool) path: section/p
#: C/general.page:16
#, fuzzy
#| msgid ""
#| "Metadata within a file can tell a lot about you. Cameras record data "
#| "about when a picture was taken and what camera was used. Office "
#| "aplications automatically add author and company information to documents "
#| "and spreadsheets. Maybe you don't want to disclose those informations."
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"Metadata i en fil kan fortelle mye om deg. Kameraer tar opp mye data når et "
"bilde knipses, sågar også hvilket kamera som brukes. Kontorprogrammer legger "
"automatisk til forfatter og bedriftsinfo i dokumenter og regneark. Kanskje "
"du ikke ønsker å dele dette?"

#. (itstool) path: figure/title
#: C/general.page:18
msgid "Example of metadata in a picture file"
msgstr "Eksempel på metadata i en bildefil"

#. (itstool) path: figure/desc
#: C/general.page:20
#, fuzzy
#| msgid ""
#| "The file discloses informations about the hardware, settings, date, "
#| "location of the shoot. It contains a total of 79 metadata."
msgid ""
"The file discloses information about the hardware, settings, date, location "
"of the shoot. It contains a total of 79 metadata."
msgstr ""
"Filen tilkjennegir info om maskinvaren, innstillinger, dato, og stedet "
"bildet ble tatt. I alt er det 79 biter metadata."

#. (itstool) path: section/title
#: C/general.page:27
msgid "Cleaning process"
msgstr "Renseprosessen"

#. (itstool) path: section/p
#: C/general.page:28
#, fuzzy
#| msgid ""
#| "While <app>Metadata Cleaner</app> is doing its very best to display "
#| "metadata, it doesn't mean that a file is clean from any metadata if it "
#| "doesn't show any. There is no reliable way to detect every single "
#| "possible metadata for complex file formats. This is why you shouldn't "
#| "rely on metadata's presence to decide if your file must be cleaned or not."
msgid ""
"While <app>Metadata Cleaner</app> is doing its very best to display "
"metadata, it doesn't mean that a file is clean from metadata if it doesn't "
"show any. There is no reliable way to detect every single possible metadata "
"for complex file formats. This is why you shouldn't rely on metadata's "
"presence to decide if your file must be cleaned or not."
msgstr ""
"Selv om <app>Metadatarenseren</app> gjør sitt beste for å vise metadata, "
"betyr det ikke at en fil er fri for metadata hvis den ikke vises. Det er "
"ingen pålitelige måter å oppdage hver enkelt metadata for komplekse "
"filformater. Det er derfor du ikke bør stole på metadataens tilstedeværelse "
"for hvorvidt den bør renses eler ei."

#. (itstool) path: section/p
#: C/general.page:29
#, fuzzy
msgid ""
"<app>Metadata Cleaner</app> takes the content of the file and puts it into a "
"new one without metadata, ensuring that any undetected metadata is stripped."
msgstr ""
"<app>Metadatarenseren</app> tar innholdet i filen og putter det inn i en ny "
"uten metadata, noe som forsikrer at enhver uoppdaget metadata er strippet."

#. (itstool) path: section/title
#: C/general.page:35
msgid "Limitations"
msgstr "Begrensninger"

#. (itstool) path: section/p
#: C/general.page:36
#, fuzzy
#| msgid ""
#| "Be aware that metadata are not the only way of marking a file. If the "
#| "content discloses personal informations or has been watermarked, "
#| "<app>Metadata Cleaner</app> will not protect you."
msgid ""
"Be aware that metadata is not the only way of marking a file. If the content "
"itself discloses personal information or has been watermarked, traditionally "
"or via steganography, <app>Metadata Cleaner</app> will not protect you."
msgstr ""
"Husk at metadata ikke er den eneste måten å markere en fil. Hvis innholdet "
"bringer til veie personvernsinfo eller har blitt vannmerket, vil ikke "
"<app>Metadatarenseren</app> beskytte deg."

#. (itstool) path: page/title
#. The application name. It can be translated.
#: C/index.page:10
msgid "Metadata Cleaner"
msgstr "Metadatarenser"

#. (itstool) path: page/p
#: C/index.page:14
#, fuzzy
#| msgid ""
#| "<app>Metadata Cleaner</app> allows you to view metadata in your files and "
#| "to get rid of them, as much as possible."
msgid ""
"<app>Metadata Cleaner</app> allows you to view metadata in your files and to "
"get rid of it, as much as possible."
msgstr ""
"<app>Metadatarenseren</app> lar deg å vise metadata i filene dine, og å "
"kvitte deg med den, sålangt det er mulig."

#. (itstool) path: section/title
#: C/index.page:17
#, fuzzy
#| msgid "General informations"
msgid "General information"
msgstr "Generell info"

#. (itstool) path: section/title
#: C/index.page:21
msgid "Using <app>Metadata Cleaner</app>"
msgstr "Bruk av <app>Metadata</app>"

#. (itstool) path: section/title
#: C/usage.page:14
msgid "Adding files"
msgstr "Tillegg av filer"

#. (itstool) path: section/p
#: C/usage.page:15
msgid ""
"To add files, press the <gui style=\"button\">Add Files</gui> button. A file "
"chooser will open, select the files you want to clean."
msgstr ""
"Klikk <gui style=\"button\">Legg til filer</gui>-knappen med de filene du "
"vil rense. En filvelger vil åpnes, og du kan velge filene du ønsker å rense."

#. (itstool) path: figure/title
#: C/usage.page:17
msgid "<gui style=\"button\">Add Files</gui> button"
msgstr "<gui style=\"button\">Legg til filer</gui>-knappen"

#. (itstool) path: section/p
#: C/usage.page:20
msgid ""
"To add whole folders at once, press the arrow next to the <gui style=\"button"
"\">Add Files</gui> button and press the <gui style=\"button\">Add Folders</"
"gui> button. A file chooser will open, select the folders you want to add. "
"You can optionally choose to also add all files from all subfolders by "
"checking the <gui style=\"checkbox\">Add files from subfolders</gui> "
"checkbox."
msgstr ""
"Legg til hele mapper samtidig med pilknappen ved siden av <gui style=\"button"
"\">Legg til filer</gui>-knappen og trykk <gui style=\"button\">Legg til "
"mapper</gui>-knappen. En filvelger vil åpnes, der du kan velge mappene du "
"vil legge til. Du kan alternativt velge å også legge til alle filer fra "
"undermapper ved å huke av <gui style=\"checkbox\">Legg til filer fra "
"undermapper</gui>-avkryssningsboksen."

#. (itstool) path: section/title
#: C/usage.page:26
msgid "Viewing metadata"
msgstr "Visning av metadata"

#. (itstool) path: section/p
#: C/usage.page:27
#, fuzzy
#| msgid ""
#| "Click on a file in the list to open the detailed view. If it has "
#| "metadata, they will be shown there."
msgid ""
"Click on a file in the list to open the detailed view. If it has metadata, "
"it will be shown there."
msgstr ""
"Klikk på en fil i listen for å åpne den detaljerte visingen. Hvis den har "
"matadata, vil den bli vist her."

#. (itstool) path: figure/title
#: C/usage.page:29
msgid "Detailed view of the metadata"
msgstr "Detaljert visning av metadataen"

#. (itstool) path: section/title
#: C/usage.page:37
msgid "Cleaning files"
msgstr "Rensing av filer"

#. (itstool) path: section/p
#: C/usage.page:38
msgid ""
"To clean all the files in the window, press the <gui style=\"button\">Clean</"
"gui> button. The cleaning process may take some time to complete."
msgstr ""
"For å rense filene i vinduet, trykker du på <gui style=\"button\">Rens</gui>-"
"knappen. Det kan ta litt tid å fullføre renseprosessen."

#. (itstool) path: figure/title
#: C/usage.page:40
msgid "<gui style=\"button\">Clean</gui> button"
msgstr "<gui style=\"button\">Rens</gui>-knappen"

#. (itstool) path: section/title
#: C/usage.page:48
msgid "Lightweight cleaning"
msgstr "Lett rensing"

#. (itstool) path: section/p
#: C/usage.page:49
#, fuzzy
msgid ""
"By default, the removal process might alter a bit the data of your files, in "
"order to remove as much metadata as possible. For example, texts in PDF "
"might not be selectable anymore, compressed images might get compressed "
"again…"
msgstr ""
"Som forvalg kan det hende fjerningsprosessen endrer filene dine litt for å "
"fjerne så mye metadata som mulig. For eksempel kan det hende tekst ikke kan "
"velges i PDF-er, og at sammenpakkede bilder ikke kan komprimeres igjen …"

#. (itstool) path: section/p
#: C/usage.page:50
msgid ""
"The lightweight mode, while not as thorough, will not make destructive "
"changes to your files."
msgstr ""
"Lettvektsmodus, som ikke er like grundig, og ikke gjør noen destruktive "
"endringer i noen av filene dine."

#, fuzzy
#~ msgid ""
#~ "If you're willing to trade some metadata's presence in exchange of the "
#~ "guarantee that the data of your files won't be modified, the lightweight "
#~ "mode precisely does that."
#~ msgstr ""
#~ "Den lette renseprosessen gjør derimot ingen skadelige endringer, men er "
#~ "ikke like grundig."
