# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the fr.romainvigier.MetadataCleaner package.
# Ihor Hordiichuk <igor_ck@outlook.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: fr.romainvigier.MetadataCleaner\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-07-03 13:24+0200\n"
"PO-Revision-Date: 2022-07-04 15:51+0000\n"
"Last-Translator: Ihor Hordiichuk <igor_ck@outlook.com>\n"
"Language-Team: Ukrainian <https://hosted.weblate.org/projects/"
"metadata-cleaner/application/uk/>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.13.1-dev\n"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:6
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:7
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:9
#: application/data/ui/AboutDialog.ui:76 application/metadatacleaner/app.py:38
msgid "Metadata Cleaner"
msgstr "Очищувач метаданих"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:8
msgid "Clean metadata from your files"
msgstr "Очистьте метадані своїх файлів"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:15
msgid "Metadata;Remover;Cleaner;"
msgstr "Метадані;Очищувач;Metadata;Remover;Cleaner;"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:10
msgid "Clean without warning"
msgstr "Очищувати без попередження"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:11
msgid "Clean the files without showing the warning dialog"
msgstr "Очищення файлів без показу діалогового вікна попередження"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:15
msgid "Lightweight cleaning"
msgstr "Легке очищення"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:16
msgid "Don't make destructive changes to files but may leave some metadata"
msgstr ""
"Не вносить деструктивні зміни у файли, але можете залишити деякі метадані"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:20
msgid "Window width"
msgstr "Ширина вікна"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:21
msgid "Saved width of the window"
msgstr "Збережена ширина вікна"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:25
msgid "Window height"
msgstr "Висота вікна"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:26
msgid "Saved height of the window"
msgstr "Збережена висота вікна"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:10
msgid "View and clean metadata in files"
msgstr "Переглянути й очистити метадані у файлах"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:27
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"Метадані у файлі можуть багато розповісти про вас. Камери записують дані про "
"те, коли та де було зроблено знімок і яка камера була використана. "
"Застосунки Office автоматично додають дані про автора та компанію в "
"документи та електронні таблиці. Це конфіденційна інформація, і ви, можливо, "
"не захочете її розголошувати."

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:28
msgid ""
"This tool allows you to view metadata in your files and to get rid of it, as "
"much as possible."
msgstr ""
"Цей засіб дає змогу переглядати метадані у ваших файлах і максимально "
"позбутися від них."

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:70
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:78
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:86
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:95
msgid "Updated translations"
msgstr "Оновлені переклади"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:94
msgid "Improved user interface"
msgstr "Покращений інтерфейс користувача"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:103
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:111
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:119
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:127
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:136
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:145
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:153
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:172
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:180
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:188
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:199
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:210
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:219
msgid "New translations"
msgstr "Нові переклади"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:135
msgid "Bug fixes"
msgstr "Виправлення помилок"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:144
msgid "New button to add folders"
msgstr "Нова кнопка для додавання тек"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:161
msgid "Improved adaptive user interface"
msgstr "Покращений адаптивний інтерфейс користувача"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:162
msgid "New help pages"
msgstr "Нові сторінки довідки"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:163
msgid "One-click cleaning, no need to save after cleaning"
msgstr ""
"Очищення одним натисканням та без необхідності зберігати після очищення"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:164
msgid "Persistent lightweight cleaning option"
msgstr "Надійна проста опція очищення"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:211
msgid "Files with uppercase extension can now be added"
msgstr "Відтепер можна додавати файли з розширенням великими буквами"

#: application/data/gtk/help-overlay.ui:15
msgid "Files"
msgstr "Файли"

#: application/data/gtk/help-overlay.ui:18
msgid "Add files"
msgstr "Додати файли"

#: application/data/gtk/help-overlay.ui:24
msgid "Add folders"
msgstr "Додати теки"

#: application/data/gtk/help-overlay.ui:30
msgid "Clean metadata"
msgstr "Очистити метадані"

#: application/data/gtk/help-overlay.ui:36
msgid "Clear all files from window"
msgstr "Очистити всі файли з вікна"

#: application/data/gtk/help-overlay.ui:44
msgid "General"
msgstr "Загальні"

#: application/data/gtk/help-overlay.ui:47
msgid "New window"
msgstr "Нове вікно"

#: application/data/gtk/help-overlay.ui:53
msgid "Close window"
msgstr "Закрити вікно"

#: application/data/gtk/help-overlay.ui:59
msgid "Quit"
msgstr "Вийти"

#: application/data/gtk/help-overlay.ui:65
msgid "Keyboard shortcuts"
msgstr "Комбінації клавіш"

#: application/data/gtk/help-overlay.ui:71
msgid "Help"
msgstr "Довідка"

#: application/data/ui/AboutDialog.ui:38
msgid "About"
msgstr "Про застосунок"

#: application/data/ui/AboutDialog.ui:97
msgid "Chat on Matrix"
msgstr "Поспілкуватися в Matrix"

#: application/data/ui/AboutDialog.ui:107
msgid "View the code on GitLab"
msgstr "Переглянути код на GitLab"

#: application/data/ui/AboutDialog.ui:117
msgid "Translate on Weblate"
msgstr "Перекласти на Weblate"

#: application/data/ui/AboutDialog.ui:127
msgid "Support us on Liberapay"
msgstr "Підтримати нас на Liberapay"

#: application/data/ui/AboutDialog.ui:148
msgid "Credits"
msgstr "Подяки"

#: application/data/ui/AboutDialog.ui:170
msgid "Code"
msgstr "Код"

#: application/data/ui/AboutDialog.ui:176
msgid "Artwork"
msgstr "Ілюстрації"

#: application/data/ui/AboutDialog.ui:182
msgid "Documentation"
msgstr "Документація"

#: application/data/ui/AboutDialog.ui:188
msgid "Translation"
msgstr "Переклад"

#: application/data/ui/AboutDialog.ui:193
msgid ""
"This program uses <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> to "
"parse and clean the metadata. Show them some love!"
msgstr ""
"Цей застосунок використовує <a href=\"https://0xacab.org/jvoisin/"
"mat2\">mat2</a> для аналізу та очищення метаданих. Викажіть їм трохи "
"прихильності!"

#: application/data/ui/AboutDialog.ui:206
msgid ""
"The source code of this program is released under the terms of the <a href="
"\"https://www.gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 or later</a>. The "
"original artwork and translations are released under the terms of the <a "
"href=\"https://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA 4.0</a>."
msgstr ""
"Джерельний код цього застосунку випущено за умовами <a href=\"https://www."
"gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 або новішої</a>. Оригінал "
"ілюстрації та переклади випущені згідно з умовами <a href=\"https://"
"creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA 4.0</a>."

#: application/data/ui/AddFilesButton.ui:16
msgid "_Add Files"
msgstr "_Додати файли"

#: application/data/ui/AddFilesButton.ui:29
msgid "Add _Folders"
msgstr "Додати _теки"

#: application/data/ui/CleanMetadataButton.ui:9
msgid "_Clean"
msgstr "_Очистити"

#: application/data/ui/CleaningWarningDialog.ui:9
msgid "Make sure you backed up your files!"
msgstr "Переконайтеся, що ви створили резервну копію своїх файлів!"

#: application/data/ui/CleaningWarningDialog.ui:10
msgid "Once the files are cleaned, there's no going back."
msgstr "Після очищення, можливості відновлення файлів не буде."

#: application/data/ui/CleaningWarningDialog.ui:17
msgid "Don't tell me again"
msgstr "Не нагадувати знову"

#: application/data/ui/CleaningWarningDialog.ui:25
msgid "Cancel"
msgstr "Скасувати"

#: application/data/ui/CleaningWarningDialog.ui:30
msgid "Clean"
msgstr "Очистити"

#: application/data/ui/EmptyView.ui:13
msgid "Clean Your Traces"
msgstr "Очистьте свої сліди"

#: application/data/ui/EmptyView.ui:39
msgid "Learn more about metadata and the cleaning process limitations"
msgstr "Докладніше про метадані та обмеження процесу очищення"

#: application/data/ui/FileRow.ui:12
msgid "Remove file from list"
msgstr "Вилучити файл зі списку"

#: application/data/ui/FileRow.ui:124
msgid "Warning"
msgstr "Попередження"

#: application/data/ui/FileRow.ui:142
msgid "Error"
msgstr "Помилка"

#: application/data/ui/FileRow.ui:182
msgid "Cleaned"
msgstr "Очищено"

#: application/data/ui/MenuButton.ui:10
msgid "_New Window"
msgstr "_Нове вікно"

#: application/data/ui/MenuButton.ui:14
msgid "_Clear Window"
msgstr "_Очистити вікно"

#: application/data/ui/MenuButton.ui:20
msgid "_Help"
msgstr "_Довідка"

#: application/data/ui/MenuButton.ui:25
msgid "_Keyboard Shortcuts"
msgstr "_Комбінації клавіш"

#: application/data/ui/MenuButton.ui:29
msgid "_About Metadata Cleaner"
msgstr "_Про очищувач метаданих"

#: application/data/ui/MenuButton.ui:38
msgid "Main menu"
msgstr "Головне меню"

#: application/data/ui/SettingsButton.ui:9
msgid "Cleaning settings"
msgstr "Налаштування очищення"

#: application/data/ui/SettingsButton.ui:27
msgid "Lightweight Cleaning"
msgstr "Легке чищення"

#: application/data/ui/SettingsButton.ui:32
msgid "Learn more about the lightweight cleaning"
msgstr "Докладніше про легке очищення"

#: application/data/ui/Window.ui:78
msgid "Details"
msgstr "Подробиці"

#: application/data/ui/Window.ui:84
msgid "Close"
msgstr "Закрити"

#. Translators: Replace `translator-credits` by your name, and optionally your email address between angle brackets (Example: `Name <mail@example.org>`). If names are already present, do not remove them and add yours on a new line.
#: application/data/ui/Window.ui:104
msgid "translator-credits"
msgstr "`Ihor Hordiichuk <igor_ck@outlook.com>`"

#: application/data/ui/Window.ui:113
msgid "Choose files to clean"
msgstr "Виберіть файли для очищення"

#: application/data/ui/Window.ui:123
msgid "Choose folders to clean"
msgstr "Виберіть теки для очищення"

#: application/metadatacleaner/modules/file.py:254
msgid "Something bad happened during the cleaning, cleaned file not found"
msgstr "Під час очищення сталося щось погане, очищений файл не знайдено"

#: application/metadatacleaner/ui/detailsview.py:58
msgid "The File Has Been Cleaned"
msgstr "Файл очищено"

#: application/metadatacleaner/ui/detailsview.py:60
msgid ""
"Known metadata have been removed, however the cleaning process has some "
"limitations."
msgstr ""
"Відомі метадані були вилучені, однак процес очищення має деякі обмеження."

#: application/metadatacleaner/ui/detailsview.py:63
msgid "Learn more"
msgstr "Докладніше"

#: application/metadatacleaner/ui/detailsview.py:76
msgid "Unable to Read the File"
msgstr "Не вдалося прочитати файл"

#: application/metadatacleaner/ui/detailsview.py:77
msgid "File Type not Supported"
msgstr "Тип файлу не підтримується"

#: application/metadatacleaner/ui/detailsview.py:79
msgid "Unable to Check for Metadata"
msgstr "Не вдалося перевірити метадані"

#: application/metadatacleaner/ui/detailsview.py:80
msgid "No Known Metadata"
msgstr "Немає відомих метаданих"

#: application/metadatacleaner/ui/detailsview.py:82
msgid "Unable to Remove Metadata"
msgstr "Не вдалося вилучити метадані"

#: application/metadatacleaner/ui/detailsview.py:86
msgid "The file will be cleaned anyway to be sure."
msgstr "Про всяк випадок файл усе одно буде очищено."

#: application/metadatacleaner/ui/filechooserdialog.py:24
msgid "All supported files"
msgstr "Усі підтримувані файли"

#: application/metadatacleaner/ui/folderchooserdialog.py:22
msgid "Add files from subfolders"
msgstr "Додати файли з вкладених тек"

#: application/metadatacleaner/ui/statusindicator.py:36
msgid "Adding files…"
msgstr "Додавання файлів…"

#: application/metadatacleaner/ui/statusindicator.py:38
msgid "Processing file {}/{}"
msgstr "Обробка файлу {}/{}"

#: application/metadatacleaner/ui/statusindicator.py:40
msgid "Cleaning file {}/{}"
msgstr "Файл очищення {}/{}"
